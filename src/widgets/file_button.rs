use gettextrs::gettext;
use gtk::{gio, glib, prelude::*, subclass::prelude::*};

mod imp {
    use std::cell::RefCell;

    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate, glib::Properties)]
    #[properties(wrapper_type = super::FileButton)]
    #[template(string = r#"
    <interface>
      <template parent="GtkButton" class="FileButton">
        <property name="action-name">filebutton.choose</property>
        <child>
          <object class="AdwButtonContent" id="button_content">
            <property name="icon-name">folder-symbolic</property>
            <property name="label" translatable="yes">(None)</property>
          </object>
        </child>
      </template>
    </interface>
    "#)]
    pub struct FileButton {
        #[property(get, set, nullable)]
        pub selected_file: RefCell<Option<gio::File>>,
        #[template_child]
        pub(super) button_content: TemplateChild<adw::ButtonContent>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FileButton {
        const NAME: &'static str = "FileButton";
        type Type = super::FileButton;
        type ParentType = gtk::Button;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();

            klass.install_action_async("filebutton.choose", None, |btn, _, _| async move {
                btn.choose_file().await;
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for FileButton {}
    impl WidgetImpl for FileButton {}
    impl ButtonImpl for FileButton {}
}

glib::wrapper! {
    pub struct FileButton(ObjectSubclass<imp::FileButton>)
        @extends gtk::Widget, gtk::Button;
}

impl FileButton {
    pub async fn choose_file(&self) {
        let file_dialog = gtk::FileDialog::builder()
            .title(gettext("Save Location"))
            .build();

        let root = self.root().and_downcast::<gtk::Window>().unwrap();

        match file_dialog.select_folder_future(Some(&root)).await {
            Ok(file) => {
                let pathbuf = file.basename().unwrap();
                let basename = pathbuf.to_str().unwrap();

                self.imp().button_content.set_label(basename);
                self.set_selected_file(Some(file));
                self.notify_selected_file();
            }
            Err(err) => {
                tracing::error!("Failed to choose a file: {err}");
            }
        }
    }
}
